package com.example.rent.moviescontentresolver;

import android.content.ContentResolver;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.OnItemClick;
import butterknife.OnItemLongClick;

public class MainActivity extends AppCompatActivity {
    private static final int INDEX_ID = 0;
    private static final int INDEX_TITLE = 1;
    private static final int INDEX_DURATION = 2;
    private static final String uriString = "content://sda3.amen.com.moviesdatabase.providers.MoviesContentProvider/movies";
    private static final Uri uri = Uri.parse(uriString);

    @BindView(R.id.list_view)
    protected ListView listView;

    private ArrayAdapter<String> listAdapter;

    @OnItemLongClick(R.id.list_view)
    protected boolean removeMovie(int position) {
        String id = listAdapter.getItem(position).split(";")[0];
        ContentResolver resolver = getContentResolver();
        int i = resolver.delete(uri, "id=?",
                new String[]{id});
        Toast.makeText(this, "Removed " + i + " rows", Toast.LENGTH_SHORT).show();
        refreshMovies();
        return true;
    }

    @OnItemClick(R.id.list_view)
    protected void updateMovie(int position){

        ContentResolver resolver = getContentResolver();
//        int i = resolver.update(uri,  )

        // do skonczenia

    }

    @OnClick(R.id.button_movies_content_resolver)
    public void newMovie(){
        Intent intent = new Intent(this, AddActivity.class);
        startActivity(intent);
    }



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);

        listAdapter = new ArrayAdapter<String>(this,
                android.R.layout.simple_list_item_1,
                new ArrayList<String>());

        listView.setAdapter(listAdapter);
    }

    @Override
    protected void onResume() {
        super.onResume();
        refreshMovies();
    }

    private void refreshMovies() {
        listAdapter.clear();
        ContentResolver resolver = getContentResolver();

        Uri uri = Uri.parse(uriString);
        Cursor c = resolver.query(uri, null, null, null, null);

        for (int i = 0; i < c.getCount(); i++) {
            c.moveToNext();

            int id = c.getInt(INDEX_ID);
            String title = c.getString(INDEX_TITLE);
            int duration = c.getInt(INDEX_DURATION);

            listAdapter.add(id + ";" + title + ";" + duration );
        }
    }
}
