package com.example.rent.moviescontentresolver;
import android.content.ContentResolver;
import android.content.ContentValues;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.EditText;
import android.widget.Toast;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.OnItemClick;

public class AddActivity extends AppCompatActivity {

    private static final String uriString = "content://sda3.amen.com.moviesdatabase.providers.MoviesContentProvider/movies";
    private static final Uri uri = Uri.parse(uriString);

    @BindView(R.id.edit_title)
    protected EditText editText;

    @BindView(R.id.edit_duration)
    protected EditText editDuration;

    @OnClick(R.id.button_save)
    public void onSave(){
        String title = editText.getText().toString();
        int duration = Integer.parseInt(editDuration.getText().toString());
        ContentValues values = new ContentValues();
        values.put("name", title);
        values.put("duration", duration);

        ContentResolver resolver = getContentResolver();
        String resultUrl = resolver.insert(uri, values).toString();
        Toast.makeText(this, resultUrl, Toast.LENGTH_SHORT).show();
        finish();
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add);
        ButterKnife.bind(this);
    }
}
